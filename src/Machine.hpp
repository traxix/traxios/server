#pragma once

#include <string>

namespace traxios {

class Machine {
public:
  using ID = std::string;

private:
  ID _id;
			
public:
Machine(const ID &id) : _id(id) {}

  const ID& id() const { return _id; }
  
};

} // namespace traxios
