#pragma once

#include <sstream>
#include "common.hpp"

namespace traxios {

std::string install_sh (const std::string &id,
                        const std::string &groupid) {

  
  std::stringstream ss;
  ss << "#!/bin/bash -ex" << std::endl
     << std::endl
     << "id = " << groupid << ':' << id << std::endl
     <<
      R"Coin(

if [ "$#" -eq 1 ]; then
    id="$1:$id"
fi

echo $id
return

sudo apt install -y collectd
cd /etc/collectd
sudo curl -o ezmon.conf https://gitlab.com/traxix/ezmon/raw/master/ezmon.conf
sudo mv collectd.conf collectd.conf.old || echo "default file not found"
sudo sed "s/^ *Hostname.*/Hostname \"$id\"/" -i ezmon.conf
sudo ln -s ezmon.conf collectd.conf
sudo service collectd restart


echo ""
echo ""
echo ""
echo ""
echo "==============================================================================="
echo ""
echo ""
echo "Monitoring in place, you can visit at:"
echo "https://ezmon.traxix.net/metrics/$id"
echo ""
echo ""
echo "==============================================================================="
)Coin";

  return ss.str();

}
}  // traxios
