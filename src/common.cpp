#include "common.hpp"
#include <sstream>
#include <iostream>
#include <fstream>

namespace traxios {


std::string random_id(const int len) {
  const auto chars = "azertyuiopmlkjhgfdsqwxcvbnAZERTYUIOPMLKJHGFDSQWXCVBN0123456789";
  std::stringstream ss;
  for (int i = 0 ; i < len ; i++) {
    ss << chars[std::rand()%sizeof(chars)];
  }

  return ss.str();
}

const std::string load_file(const std::filesystem::path &path) {
  std::ifstream t(path);
  
  const std::string str((std::istreambuf_iterator<char>(t)),
                        std::istreambuf_iterator<char>());
  std::cout << t.is_open() << " loaded " <<  path.string() << " " <<  str.size() << std::endl;
  return str;
}


}  // traxios
