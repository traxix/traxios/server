#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <filesystem>
#include <glog/logging.h>
#include <nlohmann/json.hpp>
#include "Machine.hpp"
#include "Group.hpp"

namespace traxios {

using Json = nlohmann::json;

using std::int16_t;
using std::int32_t;
using std::int64_t;
using std::int8_t;

using std::uint16_t;
using std::uint32_t;
using std::uint64_t;
using std::uint8_t;

using Metric = uint32_t;
using Metrics = std::vector<Metric>;
using MetricLabel = std::string;
  
std::string random_id(const int len = 16);

const std::string load_file(const std::filesystem::path &path);

template <typename T>
T str2int (const std::string &str,
           const T on_error) {

  static_assert(std::is_integral_v<T>);
  
  T res;

  char *end = nullptr;
  if(std::is_unsigned<T>::value) {
    res = std::strtoull (str.c_str(), &end, 10);
  } else {
    res = std::strtoll(str.c_str(), &end, 10);
  }
  
  if(const_cast<const char*>(end) != (str.data() + str.size())) {
    res = on_error;
  }

  return res;
}

}  // traxios
  
