#pragma once

#include <pistache/endpoint.h>
#include <pistache/router.h>
#include <cpr/cpr.h>
#include <fmt/format.h>
#include <iostream>
#include <cerrno>

#include "Mongo.hpp"
#include "common.hpp"
#include "Machine.hpp"
#include "install.hpp"

namespace traxios {
using namespace Pistache;

class Rest {
private:
  using Request = Pistache::Rest::Request;
  using Response = Pistache::Http::ResponseWriter;

  static const int _default_window = 600;
  
  std::shared_ptr<Http::Endpoint> _httpEndpoint;
  Pistache::Rest::Router _router;
  Mongo *_db{nullptr};
  std::string _install_in;

  void init() {
    auto opts = Http::Endpoint::options().maxRequestSize(1024 * 1024).flags(Tcp::Options::ReuseAddr);
    _httpEndpoint->init(opts);
    setupRoutes();

    _install_in = load_file("install.in");
    
    start();
    std::cout << "started" << std::endl;
  }

  void start() {
    _httpEndpoint->setHandler(_router.handler());
    _httpEndpoint->serve();
  }

  void shutdown() { _httpEndpoint->shutdown(); }

  void setupRoutes() {
    using namespace ::Pistache::Rest::Routes;
    Get(_router, "/metrics/:id", bind(&Rest::metrics, this));
    Get(_router, "/group/:id", bind(&Rest::group, this));
    Get(_router, "/install.sh", bind(&Rest::install, this));
    _router.addNotFoundHandler(bind(&Rest::four04, this));
  }

  void four04(const Request &req, Response response) {
    response.send(Pistache::Http::Code::Not_Found, req.resource());
  }
  
  Json metric (const Machine::ID &id,
	       const MetricLabel &label,
	       const std::time_t start,
	       const std::time_t end,
	       const std::optional<std::pair<std::string, std::string>> match = {}
	       ) {
    Json data;
    _db->metrics(id, label, start, end, &data, match);
    return data;
  }
  
  void metrics(const Request &req, Response response) {
    const auto id = req.param(":id").as<std::string>();
    const auto query = req.query();
    const auto start = str2int(query.get("start").getOrElse("foo"), std::time(nullptr) - _default_window);
    const auto end = str2int(query.get("end").getOrElse("foo"), std::time(nullptr));
    
    Json datasets;
    datasets.push_back({{"lineTension", 0},
			{"backgroundColor", "#e3f5ffaf"},
			{"pointRadius",0},
			{"borderColor","#0074b4ff"},
			{"label","cpu"},
			{"data", metric(id, "cpu", start, end)}
      });

    datasets.push_back({{"lineTension", 0},
			{"backgroundColor", "#fff5e3af"},
			{"pointRadius",0},
			{"borderColor","#b47400ff"},
			{"label","memory"},
			{"data", metric(id, "memory", start, end, {{"type_instance", "used"}})}
      });

    //Json json ({{"datasets",{{{"label",label},{"data", data}}}}});
    Json json({{"datasets", datasets}});
    
    response.headers().add<Http::Header::AccessControlAllowOrigin>("*");
    response.headers().add<Http::Header::AccessControlAllowHeaders>("*");
    response.headers().add<Http::Header::ContentType>(MIME(Application, Json));
    const auto s = json.dump(2);
    LOG(INFO) << s.substr(0, 256) << "..." << std::endl;
    response.send(Pistache::Http::Code::Ok, s);
  }

  void group(const Request &req, Response response) {
    const auto id = req.param(":id").as<std::string>();
    Json data;
    _db->group(id, &data);
    const auto s = data.dump(2);
    response.send(Pistache::Http::Code::Ok, s);
  }

  void install(const Request &req, Response response) {
    const auto query = req.query();
    const auto groupid = query.get("groupid");
    const auto id = query.get("id");

    auto ss = install_sh(groupid.getOrElse(random_id()), id.getOrElse(random_id()));
    response.send(Http::Code::Ok, ss);
  }
  
  
public:
  Rest(uint16_t port, Mongo *db)
      : _httpEndpoint(
            std::make_shared<Http::Endpoint>(Address(Ipv4::any(), port))),
        _db(db) {

    (void)_db;
    init();
  }

  ~Rest() { shutdown(); }
};
  

}  // traxios
