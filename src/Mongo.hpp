#pragma once

#include <iostream>
#include <optional>
#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/document/value.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/options/find.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/exception/exception.hpp>
#include "common.hpp"

namespace traxios {

namespace bss = bsoncxx::builder::stream;
using bss::array;
using bss::close_array;
using bss::close_document;
using bss::concatenate;
using bss::document;
using bss::finalize;
using bss::open_array;
using bss::open_document;


mongocxx::options::find removeID() {
  mongocxx::options::find find_options;
  auto projection_doc = bss::document{} << "_id" << 0
                                        << bss::finalize;
  find_options.projection(std::move(projection_doc));
  return find_options;
}

class Mongo {
 public:
  using Collection = std::string;
  using Key = std::string;
  using Value = std::string;
  using Filters = const std::vector<std::pair<const Key& , const Value &>>;
  using Level = uint8_t;

  const uint32_t number_of_point = 256;

  
 private:
  mutable mongocxx::instance _instance;
  mutable mongocxx::uri _uri;
  mutable mongocxx::client _client;
  mutable mongocxx::database _db;
  mutable mongocxx::collection _data;


  mongocxx::options::find removeID() const {
    mongocxx::options::find find_options;
    auto projection_doc = bss::document{} << "_id" << 0
					  << bss::finalize;
    find_options.projection(std::move(projection_doc));
    return find_options;
  }
  
 public:
  Mongo(const std::string &url, const std::string &db_name)
      : _uri(url),
        _client(_uri),
        _db(_client[db_name]),
        _data(_db.collection("data")){}


  template <typename Accumulator>
  bool group(const Group::ID &id,
	     Accumulator *accumulator) {
    std::cout << "grouping for " << id << std::endl;
    auto cursor = _db["cpu"].distinct("host", bss::document{} << "hostgroup" << id << bss::finalize);

    for (const auto &doc : cursor) {
      // accumulator->push_back(doc.get_utf8().value.to_string());
      std::cout << "dist> " << bsoncxx::to_json(doc) << std::endl;
    }

    return true;
  }
  
  template <typename Accumulator>
  bool metrics(const Machine::ID &id,
	       const MetricLabel &label,
               const std::time_t start,
               const std::time_t end,
	       Accumulator *accumulator,
	       const std::optional<std::pair<std::string, std::string>> match = {}
	       ) {


    const auto aggregation_time = (end - start) / number_of_point;
    mongocxx::pipeline stages;

    auto match_ =             bss::document{}
            << "$and" << bss::open_array
            << bss::open_document << "timestamp" 
            << bss::open_document << "$gt"  << bsoncxx::types::b_date{std::chrono::milliseconds{start * 1000}} << bss::close_document
            << bss::close_document
            << bss::open_document << "timestamp" 
            << bss::open_document << "$lte" << bsoncxx::types::b_date{std::chrono::milliseconds{end * 1000}}   << bss::close_document
            << bss::close_document
            << bss::close_array
            << bss::finalize;

    const auto group =             bss::document{}
            << "_id" 
            << bss::open_document << "$subtract" << bss::open_array
            << bss::open_document << "$subtract" << bss::open_array << "$timestamp" << bsoncxx::types::b_date{std::chrono::milliseconds{0}} << bss::close_array << bss::close_document 
            << bss::open_document <<  "$mod" << bss::open_array
            << bss::open_document << "$subtract" << bss::open_array
            << "$timestamp" << bsoncxx::types::b_date{std::chrono::milliseconds{0}} << bss::close_array << bss::close_document
            << aggregation_time << bss::close_array << bss::close_document << bss::close_array << bss::close_document
            << "foo" << bss::open_document << "$avg" << "$values" << bss::close_document << bss::finalize;

    LOG(INFO) << "paris> " << bsoncxx::to_json(match_);
    LOG(INFO) << "group> " << bsoncxx::to_json(group);

    
    
    if(!match) {
      stages.match(bss::document{} << "host" << id << bss::finalize);
    } else {
      stages.match(bss::document{} << "host" << id << match->first << match->second << bss::finalize);
      std::cout << "coin" << std::endl;
    }
    stages.sort(bss::document{} << "timestamp" << 1 << bss::finalize)
        .unwind("$values")
      .match(std::move(match_))
        .group(
            bss::document{}
            << "_id" 
            << bss::open_document << "$subtract" << bss::open_array
            << bss::open_document << "$subtract" << bss::open_array << "$timestamp" << bsoncxx::types::b_date{std::chrono::milliseconds{0}} << bss::close_array << bss::close_document 
            << bss::open_document <<  "$mod" << bss::open_array
            << bss::open_document << "$subtract" << bss::open_array
            << "$timestamp" << bsoncxx::types::b_date{std::chrono::milliseconds{0}} << bss::close_array << bss::close_document
            << aggregation_time << bss::close_array << bss::close_document << bss::close_array << bss::close_document
            << "foo" << bss::open_document << "$avg" << "$values" << bss::close_document << bss::finalize)
        .sort(bss::document{} << "_id" << 1 << bss::finalize);

    auto cursor = _db[label].aggregate(stages);
    Metrics ret;
    for (auto&& doc : cursor) {
      accumulator->push_back(Json{{"x", doc["_id"].get_int64().value / 1000}, {"y", doc["foo"].get_double().value}});
    }
    
    return true;
  }

  
  
  // bool cpu (const UserID &id,
  // 	    const std::time_t aggregation_time,
  // 	    Accumulator *accumulator) {

  //   if(!metrics(id, "cpu", 
  // }
    
  
}; // class Mongo

}  // traxios
