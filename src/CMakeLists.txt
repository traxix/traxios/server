
set(CMAKE_CXX_FLAGS "-Wall ${CMAKE_CXX_FLAGS} -std=c++17 -Wno-ignored-qualifiers -pipe -Werror")
set(CMAKE_CXX_FLAGS_RELEASE " -O3")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0 -fsanitize=address")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-g -O2 -fsanitize=address")
set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH};${CMAKE_BINARY_DIR}/root/lib/cmake")

include(GNUInstallDirs)
include(FindPkgConfig)

add_executable(server
  common.cpp
  common.hpp
  Group.hpp
  Machine.hpp
  Mongo.hpp
  positions.hpp
  Rest.hpp
  server.cpp
  install.hpp
  )

#get_cmake_property(_variableNames VARIABLES)
#list (SORT _variableNames)
#foreach (_variableName ${_variableNames})
#    message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()

target_link_libraries(server pistache ${LIBBSONCXX_STATIC_LIBRARIES} ${LIBMONGOCXX_STATIC_LIBRARIES} fmt cpr::cpr glog stdc++fs)
include_directories(${LIBBSONCXX_STATIC_INCLUDE_DIRS} ${LIBMONGOCXX_STATIC_INCLUDE_DIRS} )
message(STATUS "trax> mongo include>${LIBMONGOCXX_INCLUDE_DIRS}" )
include(CPack)


install(TARGETS server
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
  )
