#pragma once

#include "Schemas.hpp"
#include <vector>

namespace traxios {

  inline const int LON = 0;
  inline const int LAT = 1;

  using MongoPositions = std::vector<MongoPosition>;
  
  void zero(ReactPosition *position) {
    position->set_accuracy(0);
    position->set_altitude(0);
    position->set_heading(0);
    position->set_speed(0);
    position->set_latitude(0);
    position->set_longitude(0);
  } 
  
  ReactPosition convert_position(const MongoPosition &mongo_position) {
    ReactPosition position;
    zero(&position);
    const auto &coordinates = mongo_position.get_location().get_coordinates();
    position.set_latitude(coordinates[LAT]); // TODO check order
    position.set_longitude(coordinates[LON]);
    mongo_position.get_location().get_coordinates();
    return position;
  }

  

}  // traxios
