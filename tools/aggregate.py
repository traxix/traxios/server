#!/usr/bin/env python3
from datetime import datetime
from datetime import timedelta
from multiprocessing.pool import ThreadPool
from time import sleep

import pymongo
import numpy as np
import pandas as pd

COLLECTIONS = ["memory", "cpu"]
RECORDS_MERGED = "records_merged"

db = pymongo.MongoClient().collectd


def create_indexes():
    for collection in COLLECTIONS + [
        RECORDS_MERGED,
        "merged_1min",
        "merged_1h",
        "merged_1d",
    ]:
        if collection not in db.list_collection_names():
            db.create_collection(collection)
        db[collection].create_index(
            [("hostgroup", 1), ("host", 1), ("aggregated", 1), ("timestamp", 1)]
        )


def timestamps_by_machine(method, collections):
    results = {}
    for collection in collections:
        aggregate = db[collection].aggregate(
            [
                {
                    "$group": {
                        "_id": {
                            "host": "$host",
                            "hostgroup": "$hostgroup",
                            "aggregated": "$aggregated",
                        },
                        "timestamp": {method: "$timestamp"},
                    }
                }
            ]
        )
        for result in aggregate:
            if result["_id"].get("aggregated"):
                continue
            key = (result["_id"].get("hostgroup", None), result["_id"]["host"])
            results.setdefault(key, result["timestamp"])
            results[key] = max(results[key], result["timestamp"])
    return results


min_timestamps_by_machine = lambda collections: timestamps_by_machine(
    "$min", collections
)
max_timestamps_by_machine = lambda collections: timestamps_by_machine(
    "$max", collections
)


def merge_records(hostgroup, host, timestamp, duration=timedelta(hours=1)):
    for collection in COLLECTIONS:
        merged_records = {}
        ids_to_delete = []
        query = {
            "hostgroup": hostgroup,
            "host": host,
            "timestamp": {"$gte": timestamp, "$lt": timestamp + duration},
        }

        for record in db[collection].find(query):
            ids_to_delete.append(record["_id"])
            merged_records.setdefault(record["timestamp"], {})[
                record.get("plugin_instance", "")
                + "_"
                + record.get("type_instance", "")
            ] = np.mean(record["values"])

        for timestamp, merged_record in merged_records.items():
            keys = {
                "timestamp": timestamp,
                "plugin": collection,
                "hostgroup": hostgroup,
                "host": host,
            }
            update = {"$set": {}}
            for key, value in merged_record.items():
                update["$set"]["values." + key] = value
            db[RECORDS_MERGED].update_one(keys, update, upsert=True)

        db[collection].delete_many({"_id": {"$in": ids_to_delete}})


def process_raw_records():
    while True:
        first_timestamps = min_timestamps_by_machine(COLLECTIONS)
        if not first_timestamps:
            sleep(1)
            continue

        for (hostgroup, host), timestamp in first_timestamps.items():
            print("processing raw records", hostgroup, host, timestamp)
            merge_records(hostgroup, host, timestamp)


def aggregate_records(hostgroup, host, timestamp, duration, input_collection):
    output_collection = "records_" + duration
    records_by_plugin = {}
    ids_to_tag = []
    query = {
        "hostgroup": hostgroup,
        "host": host,
        "timestamp": {
            "$gte": timestamp,
            "$lt": timestamp + pd.Timedelta(duration).to_pytimedelta(),
        },
    }

    for record in db[input_collection].find(query):
        ids_to_tag.append(record["_id"])
        for key, value in record["values"].items():
            records_by_plugin.setdefault(record["plugin"], {}).setdefault(
                key, []
            ).append(value)

    for plugin, records in records_by_plugin.items():
        mean_values = {key: np.mean(records[key]) for key in records}

        keys = {
            "timestamp": timestamp,
            "plugin": plugin,
            "hostgroup": hostgroup,
            "host": host,
        }

        update = {"$set": {}}
        for key, value in mean_values.items():
            update["$set"]["values." + key] = value

        db[output_collection].update_one(keys, update, upsert=True)

    db[input_collection].update_many(
        {"_id": {"$in": ids_to_tag}}, {"$set": {"aggregated": True}}
    )


def process_aggregate(duration):
    """
    * duration_str: one of "1min", "1h", "1d"
    """
    if duration == "1min":
        input_collection = RECORDS_MERGED
    elif duration == "1h":
        input_collection = "records_1min"
    elif duration == "1d":
        input_collection = "records_1h"

    while True:
        first_timestamps = min_timestamps_by_machine([input_collection])
        last_timestamps = max_timestamps_by_machine([input_collection])
        processed_something = False
        for (hostgroup, host), first_timestamp in first_timestamps.items():
            last_timestamp = last_timestamps.get((hostgroup, host))
            if last_timestamp is None or (
                pd.Timestamp(last_timestamp).floor(duration)
                == pd.Timestamp(first_timestamp).floor(duration)
            ):
                continue

            timestamp = pd.Timestamp(first_timestamp).floor(duration).to_pydatetime()
            processed_something = True
            print("aggregating ", (hostgroup, host, timestamp, duration))
            aggregate_records(hostgroup, host, timestamp, duration, input_collection)

        if not processed_something:
            sleep(1)


def main():
    print("Creating indexes")
    create_indexes()
    print("Finished creating indexes")
    with ThreadPool(4) as pool:
        futures = [
            pool.apply_async(process_raw_records, []),
            pool.apply_async(process_aggregate, ["1min"]),
            pool.apply_async(process_aggregate, ["1h"]),
            pool.apply_async(process_aggregate, ["1d"]),
        ]
        for future in futures:
            future.wait()


if __name__ == "__main__":
    main()
