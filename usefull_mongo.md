```
db.memory.aggregate([
	{"$match": {"host":"client00"}},
	{$sort:{timestamp:1}},
	{$unwind:"$values"}, 
	{$group : {
		_id: 
			{"$subtract": [{ "$subtract": [ "$timestamp", new Date("1970-01-01") ] }, 
			{ "$mod": [{ "$subtract": [ "$timestamp", new Date("1970-01-01") ] }, 1000*60*15 ]}]},
		"foo": {$avg : "$values"}}},
	{$sort : {_foo: 1}}])
```
